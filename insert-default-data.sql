
-- INSERTING MOVIE

/*
 * INSERT INTO MOVIE (mtitle, mcar, mactor, mruntime, mgenre, mdirector)
 * VALUES ('영화제목', '관람등급', '주연이름', '상영시간', 런타임(정수), '장르', '감독이름');
 */

INSERT INTO MOVIE (mtitle, mcar, mactor, mruntime, mgenre, mdirector)
VALUES  ('킬빌', 'R18', '우마 서먼', 180, '액션', '쿠엔틴타란티노');




꾼 R15 현빈 117 액션 장창원
저스티스 리그 R12 벤 애플렉 120 액션 잭 스나이더
해피 데스데이 R15 제시카 로테 96 공포 크리스토퍼 랜던
토르:라그나로크 R12 크리스 헴스워스 130 액션 타이카 와이티티
범죄도시 R19 마동석 121 액션 강윤성
키드냅 R15 할리 베리 94 액션 루이스 프리에토
인셉션 R12 레오나르도 디카프리오 147 액션 크리스토퍼 놀란
매트릭스 R12 키아누 리브스 136 SF 릴리 워쇼스키
토이스토리3 ALL 톰 행크스 102 애니메이션 리 언크리치
월-E ALL 벤 버트 104 애니메이션 앤드류 스탠튼
주토피아 ALL 지니퍼 굿윈 108 애니메이션 바이론 하워드
세 얼간이 R12 아미르 칸 171 코미디 라지쿠마르히라니
트루먼 쇼 R15 짐 캐리 103 코미디 피터 위어
죠스 R12 로이 샤이더 124 공포 스티븐 스필버그
직쏘 R19 토빈 벨 92 공포 마이클 스피어리그
그것 R15 빌 스카스가드 135 공포 안드레스 무시에티
애나벨:인형의 주인 R15 스테파니 시그만 109 공포 데이비드F.샌드버그
터미네이터2 R15 아놀드 슈왈제네거 137 SF 제임스 카메론
에이리언:커버넌트 R15 마이클 패스벤더 122 SF 리들리 스콧
라이프 R15 제이크 질렌할 103 SF 다니엘 에스피노사
어바웃 타임 R15 도널 글리슨 123 멜로 리차드 커티스
타이타닉 R15 레오나르도 디카프리오 195 멜로 제임스 카메론
범죄와의 전쟁 R19 최민식 133 범죄 윤종빈
아가씨 R19 김민희 144 스릴러 박찬욱
신세계 R19 이정재 134 범죄 박훈정
타짜 R19 조승우 139 범죄 최동훈
킹스맨:시크릿 에이전트 R19 콜린 퍼스 128 액션 매튜 본
킹스맨:골든 서클 R19 콜린 퍼스 141 액션 매튜 본











-- INSERTING CREW

/*
 * INSERT INTO EMPLOYEE (ename, ehuman)
 * VALUES ('직원이름', '사람구분');
 *
 */

INSERT INTO EMPLOYEE (ename, ehuman)
VALUES ('임시직원1', '직원');

INSERT INTO EMPLOYEE (ename, ehuman)
VALUES ('POS-001', '단말기');

INSERT INTO EMPLOYEE (ename, ehuman)
VALUES ('POS-002', '단말기');

-- INSERTING CUSTOMER

/*
 *
 * i) 회원
 *
 * WITH C AS (
 *   INSERT INTO CUSTOMER 
 *   ('전화번호', 나이(정수), '이름', '아이디', '패스워드')
 *   VALUES (phonenumber, age, cname, cid, cpw )
 *   RETURNING cnumber
 * ) INSERT INTO MEMBER (cnumber, crank, cpoint)
 * VALUES ((SELECT (C.cnumber) FROM C), '멤버십', 포인트(정수)); 
 *
 * ii) 비회원
 *
 * WITH C AS (
 *   INSERT INTO CUSTOMER (phonenumber, age, cname, cid, cpw)
 *   VALUES ('전화번호', 나이(정수), '이름', '아이디', '패스워드')
 *   RETURNING cnumber
 * ) INSERT INTO NON_MEMBER (cnumber,expiredate)
 * VALUES ((SELECT (C.cnumber) FROM C), '유효기간');
 *
 */

WITH C AS (
  INSERT INTO CUSTOMER ( phonenumber, age, cname, cid, cpw )
  VALUES ('010-0000-1010', 24, '김정걸', 'jack', '123456')
  RETURNING cnumber
) INSERT INTO MEMBER (cnumber, crank, cpoint)
VALUES ((SELECT (C.cnumber) FROM C), 'VIP', 8080);

WITH C AS (
  INSERT INTO CUSTOMER ( phonenumber, age, cname, cid, cpw )
  VALUES ('010-1230-2222', 24, '이재열', 'm_01012302222', '1qaz2wsx3edc' )
  RETURNING cnumber
) INSERT INTO NON_MEMBER ( cnumber, expiredate )
VALUES ((SELECT (C.cnumber) FROM C), '2017-12-30');




010-1212-2121 18 이시우 lcw1212 33333333
3 GOLD 500


010-5532-1231 35 이석현 hyun1231 1132333
4 SILVER 240


010-7633-1143 35 박경진 jin1143 qwer1234
5 VIP 6300


010-5633-3343 25 도찬호 ho3341 qqhd1237
6 GOLD 3600


010-9983-3323 17 민정환 mjh9983 hwan1163
7 BRONZE 80


010-1208-9954 27 임미연 mee9954 aldus1208
8 SILVER 1200


010-2268-9394 22 진수연 m_53217643212 6jkl3nuv2fdq
9 2017-12-15


010-8532-1112 28 차중호 m_18547678542 9qen3kei2wsa
10 2017-11-13

010-2508-7785 24 김다솜 m_25489678541 5qwu2xsd1qwe
11 2017-11-22


010-2662-3221 26 김효진 m_77854468795 8fsc2qwy7cfs
12 2017-12-05


010-9987-2215 18 곽성훈 m_77854467845 8fsc2qwu7rre
13 2017-12-12


010-1123-4456 31 김지인 m_77854467995 8fsc2qwu1qpo
14 2017-12-12


010-8843-1123 41 윤기주 m_77985412563 8fsc5wqs1ddw
15 2017-12-27


010-2156-8546 22 오준혁 m_77985415874 8fsc5wqs2qoi
16 2017-12-27


010-6539-1187 22 김보영 m_77985558749 8fsc6gbc1aqw
17 2017-12-28

010-6221-6765 28 최현우 m_77981854989 8fsc8mmj4poi
18 2017-12-30



010-8875-1297 27 오은주 joo1297 dmswn1297
19 GOLD 5600


010-4456-5852 25 우민성 minsung5852 alstjd6621
20 SILVER 2700

010-1109-0876 18 권보람 boram0876 qhfka1109
21 BRONZE 700



010-7454-0542 31 손정훈 hooon0542 thswjdgns112
22 BRONZE 1000


010-1221-7432 29 현예림 hyleem7432 dpfla7292
23 VIP 10500


010-5052-7560 22 우민성 minsung7560 alstjd123e32
24 GOLD 7900

