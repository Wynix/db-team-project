-- CREATING TABLE

DROP TABLE IF EXISTS RESERVATION CASCADE;
DROP TABLE IF EXISTS PRINT CASCADE;
DROP TABLE IF EXISTS PLAN CASCADE;
DROP TABLE IF EXISTS INFORMATION CASCADE;
DROP TABLE IF EXISTS MOVIE;
DROP TABLE IF EXISTS SCREENING_TABLE;
DROP TABLE IF EXISTS CUSTOMER CASCADE;
DROP TABLE IF EXISTS MEMBER;
DROP TABLE IF EXISTS NON_MEMBER;
DROP TABLE IF EXISTS TICKET;
DROP TABLE IF EXISTS EMPLOYEE;

CREATE TABLE IF NOT EXISTS MOVIE (
       mpnumber  SERIAL NOT NULL PRIMARY KEY,
       mtitle	 VARCHAR(20) NOT NULL,
       mcar	 VARCHAR(10) NOT NULL,
       mactor    VARCHAR(20) NOT NULL,
       mruntime  INTEGER NOT NULL,
       mgenre    VARCHAR(10) NOT NULL,
       mdirector VARCHAR(20) NOT NULL
);

CREATE TABLE IF NOT EXISTS SCREENING_TABLE  (
       stnumber  SERIAL NOT NULL PRIMARY KEY,
       starttime TIME NOT NULL UNIQUE,
       period    DATE NOT NULL,
       capacity  INTEGER NOT NULL,
       audnumber INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS CUSTOMER (
       cnumber	      SERIAL NOT NULL PRIMARY KEY,
       phonenumber    VARCHAR(20) NOT NULL,
       age	      INTEGER NOT NULL,
       cname	      VARCHAR(20) NOT NULL,
       cid	      VARCHAR(20) NOT NULL,
       cpw	      VARCHAR(20) NOT NULL
);

CREATE TABLE IF NOT EXISTS MEMBER (
       mnumber	        SERIAL NOT NULL,
       cnumber		INTEGER NOT NULL,
       crank 	  	VARCHAR(20) NOT NULL,
       cpoint     	INTEGER	NOT NULL,

       FOREIGN KEY (cnumber) REFERENCES CUSTOMER(cnumber)
);

CREATE TABLE IF NOT EXISTS NON_MEMBER (
       nmnumber     SERIAL NOT NULL,
       cnumber	    INTEGER NOT NULL,
       expiredate   DATE    NOT NULL,

       FOREIGN KEY (cnumber) REFERENCES CUSTOMER(cnumber)
);

CREATE TABLE IF NOT EXISTS TICKET (
       tnumber	    SERIAL 	  UNIQUE NOT NULL,
       seatnumber   INTEGER    	  UNIQUE NOT NULL,
       tprice	    INTEGER 	  NOT NULL,
       discount	    VARCHAR(20)   NOT NULL,
       pmethod      VARCHAR(20)   NOT NULL,
       age	    VARCHAR(20)   NOT NULL
);

CREATE TABLE IF NOT EXISTS EMPLOYEE (
       enumber	    SERIAL   	     UNIQUE NOT NULL,
       ename	    VARCHAR(20)      NOT NULL,
       ehuman	    VARCHAR(20)      NOT NULL
);

CREATE TABLE IF NOT EXISTS PLAN (
       pnumber	    SERIAL 	NOT NULL,
       mpnumber	    INTEGER	NOT NULL,
       stnumber	    INTEGER	NOT NULL,

       FOREIGN KEY (mpnumber) REFERENCES MOVIE(mpnumber),
       FOREIGN KEY (stnumber) REFERENCES SCREENING_TABLE(stnumber)

);

CREATE TABLE IF NOT EXISTS RESERVATION (
       rnumber	    SERIAL  	NOT NULL,
       cnumber	    INTEGER	NOT NULL,
       tnumber	    INTEGER	NOT NULL,

       FOREIGN KEY (cnumber) REFERENCES CUSTOMER(cnumber),
       FOREIGN KEY (tnumber) REFERENCES TICKET(tnumber)
);

CREATE TABLE IF NOT EXISTS PRINT (
       prnumber	    SERIAL 	 NOT NULL,
       enumber	    INTEGER	 NOT NULL,
       tnumber	    INTEGER	 NOT NULL,

       FOREIGN KEY (enumber) REFERENCES EMPLOYEE(enumber),
       FOREIGN KEY (tnumber) REFERENCES TICKET(tnumber)
);

CREATE TABLE IF NOT EXISTS INFORMATION (
       inumber	    SERIAL 	 NOT NULL,
       tnumber	    INTEGER	 NOT NULL,
       mpnumber	    INTEGER	 NOT NULL,

       FOREIGN KEY (tnumber) REFERENCES TICKET(tnumber),
       FOREIGN KEY (mpnumber) REFERENCES MOVIE(mpnumber) 
);
-- ADDING CONSTRAINT

-- INSERTING MOVIE

/*
 * INSERT INTO MOVIE (mtitle, mcar, mactor, mruntime, mgenre, mdirector)
 * VALUES ('영화제목', '관람등급', '주연이름', '상영시간', 런타임(정수), '장르', '감독이름');
 */

INSERT INTO MOVIE (mtitle, mcar, mactor, mruntime, mgenre, mdirector)
VALUES  ('킬빌', 'R18', '우마 서먼', 180, '액션', '쿠엔틴타란티노');


-- INSERTING CREW

/*
 * INSERT INTO EMPLOYEE (ename, ehuman)
 * VALUES ('직원이름', '사람구분');
 *
 */

INSERT INTO EMPLOYEE (ename, ehuman)
VALUES ('임시직원1', '직원');

INSERT INTO EMPLOYEE (ename, ehuman)
VALUES ('POS-001', '단말기');

INSERT INTO EMPLOYEE (ename, ehuman)
VALUES ('POS-002', '단말기');

-- INSERTING CUSTOMER

/*
 *
 * i) 회원
 *
 * WITH C AS (
 *   INSERT INTO CUSTOMER 
 *   ('전화번호', 나이(정수), '이름', '아이디', '패스워드')
 *   VALUES (phonenumber, age, cname, cid, cpw )
 *   RETURNING cnumber
 * ) INSERT INTO MEMBER (cnumber, crank, cpoint)
 * VALUES ((SELECT (C.cnumber) FROM C), '멤버십', 포인트(정수)); 
 *
 * ii) 비회원
 *
 * WITH C AS (
 *   INSERT INTO CUSTOMER (phonenumber, age, cname, cid, cpw)
 *   VALUES ('전화번호', 나이(정수), '이름', '아이디', '패스워드')
 *   RETURNING cnumber
 * ) INSERT INTO NON_MEMBER (cnumber,expiredate)
 * VALUES ((SELECT (C.cnumber) FROM C), '유효기간');
 *
 */

WITH C AS (
  INSERT INTO CUSTOMER ( phonenumber, age, cname, cid, cpw )
  VALUES ('010-0000-1010', 24, '김정걸', 'jack', '123456')
  RETURNING cnumber
) INSERT INTO MEMBER (cnumber, crank, cpoint)
VALUES ((SELECT (C.cnumber) FROM C), 'VIP', 8080);

WITH C AS (
  INSERT INTO CUSTOMER ( phonenumber, age, cname, cid, cpw )
  VALUES ('010-1230-2222', 24, '이재열', 'm_01012302222', '1qaz2wsx3edc' )
  RETURNING cnumber
) INSERT INTO NON_MEMBER ( cnumber, expiredate )
VALUES ((SELECT (C.cnumber) FROM C), '2017-12-30');
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET (seatnumber, tprice, discount, pmethod, age) VALUES (1, 10000, '할인미적용', '카드', '성인') RETURNING tnumber ) SELECT T.tnumber FROM T );

INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), 1);

INSERT INTO RESERVATION (cnumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET (seatnumber, tprice, discount, pmethod, age) VALUES (2, 10000, '할인미적용', '카드', '성인') RETURNING tnumber ) SELECT T.tnumber FROM T );

INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), 1);

INSERT INTO RESERVATION (cnumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

DROP TABLE TMP;
